extends Area2D
export var id = 0

func _ready():
	pass


func _on_teleporter_body_entered(body):
	if !body.is_in_group("players"):
		return
		
	for t in get_tree().get_nodes_in_group("teleporter_landing"):
		if t.id == id:
			body.position = t.position
			body.velocity.y = 0
			return
