extends CanvasLayer

onready var msg = $sprite/Label
onready var timer = $Timer


func unload():
	$AnimationPlayer.play("unload")

func load():
	timer.start()
	$AnimationPlayer.play("load")
