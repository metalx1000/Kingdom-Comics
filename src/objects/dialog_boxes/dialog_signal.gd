extends Area2D

export(String, MULTILINE) var msg = "Hello There"
export var time = 3
export var onetime = true

func _on_dialog_signal_body_entered(body):
	if body.has_method("load_dialog"):
		body.load_dialog(msg,time)
		queue_free()
