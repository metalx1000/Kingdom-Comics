extends KinematicBody2D

var explosion = preload("res://objects/explosion/explosion.tscn")


var dead = false
var speed = 1000
var velocity = Vector2.ZERO
onready var screen_size = get_viewport_rect().size
var direction = "idle"

var powerup = true

func _ready():
	$AnimationPlayer.play("powerdown")
	pass

func _physics_process(delta):
	if dead:
		return
		
	get_input(delta)
	check_direction(delta)
	velocity = move_and_slide(velocity, Vector2.UP)

func check_direction(delta):
	if direction == "right":
		if !$right.is_colliding() && !$right2.is_colliding():
			var x = speed * delta * 10
			move(delta,0,x,0)
	elif direction == "left":
		if !$left.is_colliding() && !$left2.is_colliding():
			var x = speed * delta * -10
			move(delta,180,x,0)
	elif direction == "up":
		if !$up.is_colliding() && !$up2.is_colliding():
			var y = speed * delta * -10
			move(delta,-90,0,y)
	elif direction == "down":
		if !$down.is_colliding() && !$down2.is_colliding():
			var y = speed * delta * 10
			move(delta,90,0,y)
	else:
		return
		
func move(delta,degrees, x, y):
	$Sprite.rotation_degrees = degrees
	velocity.y = y
	velocity.x = x
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func power_up():
	$AnimationPlayer.play("powerup")
	powerup = true
	$Sprite.texture = load("res://objects/ship_maze/ship_powerup.png")
	#$AnimationPlayer.stop()

	$powerdown.stop()
	$powerdown.start()
	
func power_down():
	powerup = false
	$Sprite.texture = load("res://objects/ship_maze/ships.png")
	
func get_input(delta):
	if Input.is_action_just_pressed("walk_right"):
		direction = "right"

	elif Input.is_action_just_pressed("walk_left"):
		direction = "left"
	elif Input.is_action_just_pressed("player_up"):
		direction = "up"
	elif Input.is_action_just_pressed("player_down"):
		direction = "down"
		
func death():
	dead = true
	var e = explosion.instance()
	e.position = global_position
	get_parent().add_child(e)
	queue_free()


func _on_powerdown_timeout():
	$AnimationPlayer.play("powerdown")
