extends Area2D


func _ready():
	pass


func _on_pellet_body_entered(body):
	if body.is_in_group("players"):
		$AnimationPlayer.play("die")
	
func die():
	queue_free()
