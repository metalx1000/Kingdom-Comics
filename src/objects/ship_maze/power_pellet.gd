extends Area2D


func _ready():
	pass


func _on_power_pellet_body_entered(body):
	if body.is_in_group("players"):
		body.power_up()
		queue_free()
