extends KinematicBody2D

var explosion = preload("res://objects/explosion/explosion.tscn")

export var wait = 5
var direction = "left"
var speed = 800
var velocity = Vector2.ZERO
onready var screen_size = get_viewport_rect().size

func _ready():
	randomize()
	speed = rand_range(600,1000)
	$Timer.wait_time = wait
	pass
	
func _physics_process(delta):
	check_direction(delta)
	velocity = move_and_slide(velocity, Vector2.UP)
	
func check_direction(delta):
	if direction == "right":
		if !$right.is_colliding():
			var x = speed * delta * 10
			move(delta,180,x,0)
		else:
			choose_direction()
	elif direction == "left":
		if !$left.is_colliding():
			var x = speed * delta * -10
			move(delta,0,x,0)
		else:
			choose_direction()
	elif direction == "up":
		if !$up.is_colliding() && !$up2.is_colliding():
			var y = speed * delta * -10
			move(delta,90,0,y)
		else:
			choose_direction()
	elif direction == "down":
		if !$down.is_colliding() && !$down2.is_colliding():
			var y = speed * delta * 10
			move(delta,-90,0,y)
		else:
			choose_direction()
	else:
		return

func choose_direction():
	var dirs = ["right","left","up","down"]
	randomize()
	dirs.shuffle()
	direction = dirs[0]
	
	
func move(delta,degrees, x, y):
	$Sprite.rotation_degrees = degrees
	velocity.y = y
	velocity.x = x
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func death():

	var e = explosion.instance()
	e.position = global_position
	get_parent().add_child(e)
	queue_free()
	
func _on_Area2D_body_entered(body):
	if body.is_in_group("players"):
		if body.powerup:
			death()
		else:
			body.death()
		
