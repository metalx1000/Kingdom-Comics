extends Area2D

export var active = true

func end_level(body):
	if body.is_in_group("players") && active:
		
		active = false
		owner.end_level()
