extends Area2D

var explosion = preload("res://objects/explosion/explosion.tscn")
export var id = 0
func _ready():
	pass

func blow_up(truss,wait := 0.0):
	var ex = explosion.instance()
	yield(get_tree().create_timer(wait), "timeout")
	ex.global_position = truss.global_position
	owner.add_child(ex)
	truss.queue_free()
	
	
func blow_all():
	for t in get_tree().get_nodes_in_group("truss"):
		randomize()
		var w = rand_range(0.0,3.0)
		blow_up(t,w)
		
func _on_TNT_body_entered(body):
	if body.is_in_group("players"):
		if id == 100:
			get_tree().get_nodes_in_group("kong")[0].die()
			get_tree().get_nodes_in_group("players")[0].dead = true
			get_tree().get_nodes_in_group("animations")[0].play("end")
			blow_all()
			return
			
		for t in get_tree().get_nodes_in_group("truss"):
			if t.id == id:
				blow_up(t)
				yield(get_tree().create_timer(.5), "timeout")
				queue_free()
				
