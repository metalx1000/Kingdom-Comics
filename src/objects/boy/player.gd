extends KinematicBody2D

var speed = 400
var velocity = Vector2.ZERO
var dead = false

func _ready():
	pass

func _physics_process(delta):
	get_input(delta)
	velocity = move_and_slide(velocity, Vector2.UP)
	
func get_input(delta):
	var x = Input.get_action_strength("walk_right") - Input.get_action_strength("walk_left")
	var y = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	
	velocity.x = 0
	velocity.y = 0
	if x == 1:
		$Sprite.flip_h = false
		$AnimationPlayer.play("walk_right")
		velocity.x = speed * x * delta * 10 
	elif x == -1:
		$Sprite.flip_h = true
		$AnimationPlayer.play("walk_right")
		velocity.x = speed * x * delta * 10 
	elif y == 1:
		$AnimationPlayer.play("walk_down")
		velocity.y = speed * y * delta * 10 
	elif y == -1:
		$AnimationPlayer.play("walk_up")
		velocity.y = speed * y * delta * 10 
	else:
		$AnimationPlayer.play("idle")
