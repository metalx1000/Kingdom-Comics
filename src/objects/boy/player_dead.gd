extends Sprite


func _ready():
	pass

func _process(delta):
	rotation += 2*delta
	position.y += 200*delta


func _on_VisibilityNotifier2D_screen_exited():
	var player = get_tree().get_nodes_in_group("players")[0]
	player.restart()
	queue_free()
