extends Area2D

var speed = 250
var direction = 1
var parent 
var color = "red"

func _ready():
	if color == "blue":
		$Sprite.texture = load("res://objects/bullet/bluebullet_1.png")
	if direction == -1:
		$Sprite.flip_h = true
	pass # Replace with function body.

func _process(delta):
	position.x += speed * direction * delta
	
func _on_bullet_body_entered(body):
	if body == parent:
		return
		
	if body.has_method("take_damage"):
		body.take_damage()
		
	queue_free()


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()
