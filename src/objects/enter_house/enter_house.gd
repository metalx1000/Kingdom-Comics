extends Area2D
export (String,FILE,"*.tscn") var next_scene = "res://scenes/Intro.tscn"

func _ready():
	pass


func _on_enter_house_body_entered(body):
	if body.is_in_group("players"):
		body.dead = true
		Global.overworld_start_pos = body.position

		get_tree().change_scene(next_scene)
