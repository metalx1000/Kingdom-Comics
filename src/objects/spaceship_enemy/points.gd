extends Node2D

var speed = 100

func _ready():
	pass

func _physics_process(delta):
	position.y -= delta * speed

func free():
	queue_free()
