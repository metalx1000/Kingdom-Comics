extends Area2D

var speed = 200
var death = preload("res://objects/explosion/explosion.tscn")
var bullet = preload("res://objects/bullet/bullet.tscn")
var points = preload("res://objects/spaceship_enemy/points.tscn")

func _ready():
	pass

func _physics_process(delta):
	position.x -= speed * delta

func take_damage():
	var explosion = death.instance()
	explosion.position = global_position
	get_parent().add_child(explosion)
	
	var p = points.instance()
	p.position = global_position
	get_parent().add_child(p)
	
	get_parent().score += 100
	queue_free()

func shoot():
	var b = bullet.instance()
	b.parent = self
	b.speed -= 800
	b.rotation_degrees = 180
	b.position.y = global_position.y
	b.position.x = global_position.x - 20
	
	get_parent().add_child(b)

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()
