extends "res://objects/spaceship_enemy/enemy.gd"


var updown = 1

func _ready():
	randomize()
	$updown.wait_time = rand_range(1.0,2.0)
	if rand_range(0,2) > 1:
		updown *= -1

func _physics_process(delta):
	position.x -= speed * delta
	position.y += (speed/2) * delta * updown
	

func shoot():
	var b = bullet.instance()
	b.parent = self
	b.speed -= 800
	b.rotation_degrees = 180
	b.position.y = global_position.y
	b.position.x = global_position.x - 20
	
	get_parent().add_child(b)

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()


func _on_updown_timeout():
	updown *= -1
