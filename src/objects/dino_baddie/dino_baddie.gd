extends KinematicBody2D

var active = false
var dir = -1
var speed = 1000
var gravity = 3000

var velocity = Vector2.ZERO

func _ready():
	randomize()
	speed = rand_range(1000,4000)
	var t = rand_range(1,4)
	$Sprite.flip_h = true
	yield(get_tree().create_timer(t),"timeout")
	activate()
	

func _process(delta):
	if active:
		$AnimationPlayer.play("walk")
		#velocity.x = 0
		velocity.x = dir * speed * delta
		
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func turn():
	dir *= -1
	randomize()
	var turn_time = rand_range(2,5)
	$turn_timer.wait_time = turn_time
	
	if dir > 0:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true

func wake():
	$AnimationPlayer.play("wake")

func activate():
	active = true
	$turn_timer.start()

func take_damage():
	active = false
	velocity.x = 0
	$turn_timer.stop()
	$AnimationPlayer.play("damage")
