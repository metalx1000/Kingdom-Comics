extends Node2D

var barrels = preload("res://scenes/levels/kong/barrel_roll.tscn")
var active = true

func _process(delta):
	if !active:
		modulate.a = .5
		rotate(delta * 2)
		position.y += delta * 300

func die():
	active = false
	$AudioStreamPlayer2D.play()
	
func attack():
	if active:
		$AnimationPlayer.play("through")

func throw_barrel():
	var barrel = barrels.instance()
	barrel.position = $Sprite/Node2D.position
	add_child(barrel)
	reset_timer()

func reset_timer():
	randomize()
	$Timer.stop()
	$Timer.wait_time = rand_range(2,4)
	$Timer.start()
