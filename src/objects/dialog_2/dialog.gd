extends CanvasLayer

var npc = false
var paula = false

func _ready():
	$Sprite.visible = true
	pass

func load_msg(msg,wait):
	if npc:
		$Sprite/face.texture = load("res://objects/dialog_2/npc.png")
	elif paula:
		$Sprite/face.texture = load("res://objects/dialog_2/paula.jpg")
	else:
		$Sprite/face.texture = load("res://objects/dialog_2/jason.jpg")
		
	$Sprite/Label.text = msg
	$AnimationPlayer.play("in")
	$Timer.wait_time = wait
	$Timer.start()

func _on_Timer_timeout():
	$AnimationPlayer.play("out")
