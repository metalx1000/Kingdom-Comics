extends Area2D

export var onetime = false
export var npc = true
export var paula = false
export var timeout = 3
export(String, MULTILINE) var msg = ""

var msgs = [
	"What a Nice Day.",
	"You're Not From Around here, are you?",
	"I would head east if I were you.",
	"What you seek is to the east.",
	"Follow the road out of the village.",
	"Earth? Never Heard of it.",
	"'Space-Dinos'? Here we just call them Dinos",
	"I think you need to head east to find what you are looking for."
]

func _ready():
	pass	

func get_msg():
	if msg != "":
		msgs = msg.split("\n",false)
		
	randomize()
	var i = rand_range(0,msgs.size())
	return msgs[i]
	

func _on_dialog_signal_body_entered(body):
	if body.is_in_group("players"):
		var box = get_tree().get_nodes_in_group("dialog_box")[0]
		box.npc = npc
		box.paula = paula
		var m = get_msg()
		box.load_msg(m,timeout)
		if onetime:
			queue_free()
