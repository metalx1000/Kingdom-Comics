extends Area2D

var points = preload("res://objects/spaceship_enemy/points.tscn")

var msgs = [
	"Oh, This is a good one.",
	"I love this comic",
	"This is a classic",
	"Glad I found this one!",
	"Found Another One",
	"I've got to get these back to the store!"
]

func _ready():
	randomize()
	$Sprite.frame = rand_range(0,6)
	
	
func _on_comic_body_entered(body):
	if body.is_in_group("players"):
		if rand_range(0,3) > 2:
			var i = rand_range(0,msgs.size())
			if body.has_method("load_dialog"):
				body.load_dialog(msgs[i],3)
		
		var p = points.instance()
		p.position = global_position
		get_parent().add_child(p)
	
		body.score += 100
		queue_free()
