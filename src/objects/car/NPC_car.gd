extends PathFollow2D

var speed = 200
var colors = [ 0, 1, 3 ]

func _ready():
	add_to_group("npc_car")
	var i = get_tree().get_nodes_in_group("npc_car").size()
	randomize()
	var r = rand_range(5,100)
	offset = i * 1000 + r
	
	var color = colors[randi() % colors.size()]
	$car/Sprite.frame = color
	#reverse()

func _process(delta):
	set_offset(get_offset() + speed * delta)

func reverse():
	var r = rand_range(0,2)
	if r > 1:
		
		speed *= -1
		$car.rotation_degrees = -90


func _on_Area2D_body_entered(body):
	randomize()
	if rand_range(0,2) > 1:
		$car/hit_01.play()
	else:
		$car/hit_02.play()
