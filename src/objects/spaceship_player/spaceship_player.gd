extends Area2D
var speed = 200
var bullet = preload("res://objects/bullet/bullet.tscn")
var active = true
var max_bullets = 2

var damage_msgs = [
	"AAAHHHH!!!",
	"I'm hit!",
	"Look Out!!!",
	"Got to get it under control!"
]

func _ready():
	pass

func _physics_process(delta):
	get_input(delta)
	position.y = clamp(position.y,-300,300)

func get_input(delta):
	if !active:
		return
		
	if Input.is_action_just_pressed("fire"):
		shoot()
		
	var d = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	position.y += d * speed * delta
	
func shoot():
	var bcount = get_tree().get_nodes_in_group("bullets").size()
	if bcount > max_bullets:
		return
		
	var b = bullet.instance()
	b.add_to_group("bullets")
	b.color = "blue"
	b.parent = self
	b.speed += speed * 4
	b.position.y = global_position.y
	b.position.x = global_position.x + 20
	
	owner.add_child(b)

func dialog(msg):
	if msg == "damage":
		randomize()
		var i:int = randi() % damage_msgs.size()
		msg = damage_msgs[i]
		

	get_parent().get_parent().dialog(msg)

func take_damage():
	if $Tween.is_active():
		return
	
	if rand_range(0,3) > 2:
		dialog("damage")
		
	$sound.play()
	active = false
	randomize()
	var r = 360*2
	if rand_range(0,2) > 1:
		r = -360*2
		
	$Tween.interpolate_property(self,"rotation_degrees",rotation_degrees,r,1)
	$Tween.start()
	modulate.a = .5
	
func _on_Tween_tween_completed(object, key):
	$Tween.reset(self)
	modulate.a = 1
	active = true


func _on_player_area_entered(area):
	if area.is_in_group("enemies"):
		take_damage()
		area.take_damage()
