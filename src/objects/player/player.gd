extends KinematicBody2D

export (String) var player_id = "1"
export (int) var speed = 1200
export (int) var jump_speed = -48
var gravity = 3000

var velocity = Vector2.ZERO
var climbing = false
var climbing_power = 1
var start_pos
var dead = false

var bullet = preload("res://objects/bullet/bullet.tscn")

func _ready():
	if player_id == "2":
		z_index = -1
		
		#Global.player_2_pos = Vector2(position)
		$Sprite.texture = load("res://objects/player/player_2.png")
		player_id = "p" + player_id + "_"
		#if Global.number_of_players < 2:
		#	queue_free()
	else:
		player_id = ""
		
	start_pos = position
	
func _physics_process(delta):
	if dead:
		return
	
	velocity.x = 0
	get_input(delta)
	
	walk()
	if climbing:
		velocity.y = gravity * delta * climbing_power * 4
	else:
		velocity.y += gravity * delta
		
	velocity = move_and_slide(velocity, Vector2.UP)
	climb(delta)
			
func _input(event):
	if event is InputEventScreenTouch:
		jump(.001)
		
func jump(delta):
	if is_on_floor():
		$jump.play()
		velocity.y = jump_speed * delta * 1000 
	else:
		$AnimationPlayer.play("jump")

func get_input(delta):
	if Input.is_action_pressed("jump"):
		jump(delta)
		
	if Input.is_action_just_pressed("fire"):
		shoot()
		
	var dir = Input.get_action_strength("walk_right") - Input.get_action_strength("walk_left")
	velocity.x = speed * dir * delta * 10 

func shoot():
	var b = bullet.instance()
	b.parent = self
	b.position.y = position.y -3
	if $Sprite.flip_h:
		b.direction = -1
	
	b.position.x = position.x + 15 * b.direction
	
	owner.add_child(b)

func walk():
	if !is_on_floor():
		$AnimationPlayer.play("jump")
	elif velocity.x > 0:
		$AnimationPlayer.play("walk")
		$Sprite.flip_h = false
	elif velocity.x < 0:
		$AnimationPlayer.play("walk")
		$Sprite.flip_h = true
	else:
		$AnimationPlayer.play("idle")
		
func climb(delta):
	if climbing:
		climbing_power = Input.get_action_strength(player_id + "climb_down") - Input.get_action_strength(player_id + "climb_up")
	else:
		climbing_power = 1
	

func death():
	$AnimationPlayer.play("death")
	visible = false
	dead = true
	
func restart():
	visible = true
	position = start_pos
	dead = false
