extends KinematicBody2D

export var frame = 13

var color = [
	"res://objects/villager/npc.png",
	"res://objects/villager/npc_brown_green.png",
	"res://objects/villager/npc_brown_light_blue.png",
	"res://objects/villager/npc_brown_yellow.png",
	"res://objects/villager/npc_light_blue.png",
	"res://objects/villager/npc_purple.png",
	"res://objects/villager/npc_red.png"
]

func _ready():
	randomize()
	var i = rand_range(0,color.size())
	$Sprite.texture = load(color[i])
	$Sprite.frame = frame
