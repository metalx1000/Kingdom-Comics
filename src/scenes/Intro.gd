extends Control

func _ready():
	VisualServer.set_default_clear_color(Color8(0,0,0,1.0))
	$AnimationPlayer.stop(true)
	$AnimationPlayer.play("intro")
	
func _input(event):
	if event.is_action_pressed("ui_accept"):
		next()
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		
func next():
	get_tree().change_scene("res://scenes/main_title.tscn")
