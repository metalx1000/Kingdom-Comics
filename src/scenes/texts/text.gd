extends Node2D

export (String,FILE,"*.tscn") var next_scene = "res://scenes/Intro.tscn"

var a = false

func _ready():
	VisualServer.set_default_clear_color(Color8(0,0,0,1.0))
	yield(get_tree().create_timer(.2),"timeout")
	a = true

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && a:
		next()

func next():
	get_tree().change_scene(next_scene)
