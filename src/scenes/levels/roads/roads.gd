extends "res://scenes/levels/level.gd"

var npc = preload("res://objects/car/NPC_car.tscn")
var npc_count = 70
var score = 0

func _ready():
	$road.visible = true
	$road2.visible = true
	$Car/ColorRect.visible = true
	for n in npc_count:
		load_car()

func _process(delta):
	update_score()

func update_score():
	if score < $Car.score:
		score += 10
		$HUD/snd.play()
		$HUD/Label.text = "Score: " + str(score)
	
func load_car():
	for road in get_tree().get_nodes_in_group("roads"):
		var n = npc.instance()
		road.add_child(n)
	

func _on_end_level_body_entered(body):
	if body.is_in_group("players"):
		$AnimationPlayer.play("end_level")
		$Car/AnimationPlayer.play("sound_fade")
		
