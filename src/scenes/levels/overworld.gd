extends "res://scenes/levels/level.gd"

onready var player = find_node("player")

func _ready():
	var start_pos = Global.overworld_start_pos
	if start_pos != Vector2.ZERO:
		player.position.x = Global.overworld_start_pos.x
		player.position.y = Global.overworld_start_pos.y + 10


