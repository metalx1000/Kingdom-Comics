extends "res://scenes/levels/level.gd"

var player_start
var ships = preload("res://objects/ship_maze/player_ship.tscn")
func _ready():
	player_start = get_tree().get_nodes_in_group("players")[0].position

func reload_player():
	var players = get_tree().get_nodes_in_group("players")
	var size = players.size()
	if size < 1:
		var ship = ships.instance()
		ship.position = player_start
		add_child(ship)

func _physics_process(delta):
	if get_tree().get_nodes_in_group("pellets").size() < 1:
		next()
