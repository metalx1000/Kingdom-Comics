extends Area2D

export var dir = 1

func _ready():
	pass


func _on_barrel_spinner_body_entered(body):
	if body.is_in_group("barrels"):
		randomize()
		var force = rand_range(1000,3000)
		body.set_applied_torque(force * dir)
