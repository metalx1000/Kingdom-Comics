extends Area2D


func _ready():
	pass


func _on_ladder_body_entered(body):
	if body.is_in_group("players"):
		body.climbing = true


func _on_ladder_body_exited(body):
	if body.is_in_group("players"):
		body.climbing = false
