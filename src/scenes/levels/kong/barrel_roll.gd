extends RigidBody2D

export var dir = 1

func _ready():
	randomize()
	var force = rand_range(500,3000)
	set_applied_torque(force * dir)
	

func _on_hitbox_body_entered(body):
	if body.is_in_group("players"):
		body.death()

func _on_hitbox_area_entered(area):
	if area.is_in_group("barrel_killers"):
		queue_free()
