extends "res://scenes/levels/level.gd"

var speed = 200

var enemies = preload("res://objects/spaceship_enemy/enemy.tscn")
var enemies_2 = preload("res://objects/spaceship_enemy/enemy_2.tscn")
var coin_snd = preload("res://scenes/levels/coin.wav")

var score = 0
var hud_score = 0

func _ready():
	pass

func _physics_process(delta):
	$Camera2D.position.x += speed * delta

func create_enemy():
	randomize()
	if rand_range(0,3) > 1:
		var enemy = enemies.instance()
		randomize()
		var y = rand_range(-150,150)
		enemy.position.y = $Camera2D/player.global_position.y + y
		enemy.position.x = $Camera2D/player.global_position.x + 1500
		enemy.speed = rand_range(150,450)
		add_child(enemy)
	
	if rand_range(0,3) > 2:
		var enemy = enemies_2.instance()
		randomize()
		var y = rand_range(0,600)
		enemy.position.y = y
		enemy.position.x = $Camera2D/player.global_position.x + 1500
		enemy.speed = rand_range(150,450)
		add_child(enemy)

func dialog(msg):
	$dialog/jason/Label.text = msg
	$dialog/AnimationPlayer.play("load")


func update_score():
	if score >= 5000:
		$AnimationPlayer.play("fadeout")
		
	if hud_score < score:
		var snd = AudioStreamPlayer.new()
		snd.set_stream(coin_snd)
		add_child(snd)
		snd.volume_db = -10
		snd.play()
		hud_score += 10
		
		$HUD/Label.text = "Score: " + str(hud_score)
