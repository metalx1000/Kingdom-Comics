extends Node2D
export (String,FILE,"*.tscn") var next_scene = "res://scenes/Intro.tscn"
export var background_color = Color8(85,180,255,1.0)
export var camera_zoom = true



func _ready():
	VisualServer.set_default_clear_color(background_color)
	if !camera_zoom:
		$TileMap/paula/Camera2D.zoom = Vector2(1,1)

func end_level():

	$AnimationPlayer.play("end_level")


func next():
	get_tree().change_scene(next_scene)


func _on_level_end_body_entered(body):
	if body.is_in_group("players"):
		body.dead = true
		end_level()


func _on_Timer_timeout_comics():
	var player = get_tree().get_nodes_in_group("players")[0]
	var comic_count = get_tree().get_nodes_in_group("comics").size()
	if comic_count > 0:
		var msg = "I only have " + str(comic_count) + " more comics to go!"
		player.load_dialog(msg,3)
	else:
		end_level()
		
func _process(delta):
	if Input.is_action_just_pressed("next_scene"):
		next()
	

