extends Node2D

var a = false

func _ready():
	VisualServer.set_default_clear_color(Color8(0,0,0,1.0))
	yield(get_tree().create_timer(.2),"timeout")
	a = true

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && a:
		$AnimationPlayer.play("fadeout")
		
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		
func next():
	get_tree().change_scene("res://scenes/texts/text_01.tscn")


func _on_Timer_timeout():
	get_tree().change_scene("res://scenes/credits.tscn")
