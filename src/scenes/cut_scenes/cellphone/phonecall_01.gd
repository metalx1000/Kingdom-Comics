extends "res://scenes/cut_scenes/cellphone/phonecall.gd"

func _ready():
	msg = [
		"That's my cellphone!|",
		"It's Jason!|", 
		"Hello!|",
		"|Hello Paula.",
		"You won't believe it! There are Dinosaurs at the comic shoppe!|",
		"|When did you start spelling shop with an 'e'?",
		"|Nevermind. I do believe you. They are space dinos!",
		"|Infact, I'm in space now!",
		"How did you get to space?|",
		"|I would tell you, but it would take to long.",
		"|And you know what John Carmack said about stories in games.",
		"Did you just break the 4th wall? Who do you think you are,Deadpool?|",
		"|It was just a joke that is so old most people won't get it anyway.",
		"|Just stay safe and I'll be back soon.",
		"|That is if this player is any good at all.",
		"You just did it again!|"
	]

