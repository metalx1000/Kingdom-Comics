extends "res://scenes/cut_scenes/cut_scene.gd"

var current = 0
var msg = []
	

func _ready():
	VisualServer.set_default_clear_color(Color8(0,0,0,1.0))

func _process(delta):
	if Input.is_action_just_pressed("fire") || \
	Input.is_action_just_pressed("ui_accept"):
		next_msg()
		
func next_msg():
	$Timer.stop()
	$Timer.start()
	if current >= msg.size():
		$AnimationPlayer.play("fadeout")
		return
		
	var m = msg[current].split("|")
	$paula/Label.text = m[0]
	$jason/Label.text = m[1]
	
	current += 1

