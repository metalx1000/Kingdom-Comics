extends "res://scenes/levels/level.gd"

onready var box = get_tree().get_nodes_in_group("dialog_box")[0]
var current_msg = 0
var msgs = [
	"jason|Hello|3",
	"npc|Welcome Traveler|3"
]

func _ready():
	pass

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		_on_Timer_timeout()

func _on_Timer_timeout():
	if current_msg >= msgs.size():
		$AnimationPlayer2.play("faceout")
		return
		
	var  m = msgs[current_msg].split("|")
	if m[0] == "npc":
		box.npc = true
	else:
		box.npc = false
	
	var wait = int(m[2])
	box.load_msg(m[1],wait)
	current_msg += 1

