extends Node2D

export (String,FILE,"*.tscn") var next_scene = "res://scenes/Intro.tscn"


func next():
	get_tree().change_scene(next_scene)
