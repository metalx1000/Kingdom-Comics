extends Node2D

onready var file = 'res://credits.txt'

func _ready():
	load_file(file)
	

func _process(delta):
	var cpos = $Camera2D.position.y
	var text_l = $Label.margin_bottom + 600
	$Label/PaulaFullSheet.position.y = text_l
	
	if Input.is_action_pressed("player_down")&& cpos < text_l:
		$Timer.stop()
		$Timer.start()
		$Camera2D.position.y += 100 * delta
		
	if Input.is_action_pressed("player_up") && $Camera2D.position.y > 300:
		$Timer.stop()
		$Timer.start()
		$Camera2D.position.y -= 100 * delta
		
	if Input.is_action_just_pressed("ui_cancel"):
		next()
		
func next():
	get_tree().change_scene("res://scenes/Intro.tscn")
	
func load_file(file):

	var f = File.new()
	f.open(file, File.READ)
	$Label.text = f.get_as_text()
	f.close()
	return
